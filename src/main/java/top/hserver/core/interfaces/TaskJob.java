package top.hserver.core.interfaces;

public interface TaskJob {

    void exec(Object... args);

}
