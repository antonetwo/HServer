package top.hserver.core.interfaces;

public interface InitRunner {
    void init(String[] args);
}
