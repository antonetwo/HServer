package top.hserver.core.ioc.annotation.apidoc;

public enum DataType {
  String,
  Integer,
  Double,
  Boolean,
  File;
  private DataType() {
  }
}